#include "shader.h"

#include <GL/glew.h>

#include "log.h"
#include "globals.h"

#include <string>
#include <vector>


Shader::Shader()
{
    GLint result = GL_FALSE;
    GLuint vertexShaderId   = glCreateShader(GL_VERTEX_SHADER),
           fragmentShaderId = glCreateShader(GL_FRAGMENT_SHADER);

    int infoLogLength;

    // create the shader program
    this->d_program = glCreateProgram();

    const char *vertexShader =
            "#version 330 core\n"

            "layout(location = 0) in vec3 inPos;\n"
            "layout(location = 1) in vec3 vertexColor; \n"

            "uniform mat4 mvp; \n"

            "out vec3 fragmentColor; \n"

            "void main() {\n"
            "   gl_Position = mvp * vec4(inPos, 1); \n"
            "   fragmentColor = vertexColor; \n"
            "}";

    const char *fragmentShader =
            "#version 330 core \n"

            "in vec3 fragmentColor; \n"

            "out vec3 color; \n"

            "void main() { \n"
            "   color = fragmentColor; \n"
            "}";

    // hand over the vertex shader sources to opengl and let it compile them
    glShaderSource(vertexShaderId, 1, &vertexShader , NULL);
    glCompileShader(vertexShaderId);

    // success?
    glGetShaderiv(vertexShaderId, GL_COMPILE_STATUS, &result);
    glGetShaderiv(vertexShaderId, GL_INFO_LOG_LENGTH, &infoLogLength);

    // got some errors?
    if (infoLogLength > 0)
    {
        std::vector<char> vertexShaderErrorMessage(infoLogLength + 1);

        glGetShaderInfoLog(vertexShaderId, infoLogLength, NULL, &vertexShaderErrorMessage[0]);

        Log::log("OpenGL wants to tell us something:\n\t" + std::string(&vertexShaderErrorMessage[0]));
    }

    // hand over the fragment shader sources to opengl and compile them
    glShaderSource(fragmentShaderId, 1, &fragmentShader , NULL);
    glCompileShader(fragmentShaderId);

    // success?
    glGetShaderiv(fragmentShaderId, GL_COMPILE_STATUS, &result);
    glGetShaderiv(fragmentShaderId, GL_INFO_LOG_LENGTH, &infoLogLength);

    // got some errors?
    if (infoLogLength > 0)
    {
        std::vector<char> fragmentShaderErrorMessage(infoLogLength + 1);

        glGetShaderInfoLog(fragmentShaderId, infoLogLength, NULL, &fragmentShaderErrorMessage[0]);

        Log::log("OpenGL wants to tell us something:\n\t" + std::string(&fragmentShaderErrorMessage[0]));
    }

    // upload the shaders to the graphics core
    glAttachShader(this->d_program, vertexShaderId);
    glAttachShader(this->d_program, fragmentShaderId);

    // link the shaders into the program
    glLinkProgram(this->d_program);

    // assign some static locations
    glBindFragDataLocation(this->d_program, 0, "out0");
    glBindFragDataLocation(this->d_program, 1, "out1");
    glBindFragDataLocation(this->d_program, 2, "out2");
    glBindFragDataLocation(this->d_program, 3, "out3");
    glBindFragDataLocation(this->d_program, 4, "out4");
    glBindFragDataLocation(this->d_program, 5, "out5");
    glBindFragDataLocation(this->d_program, 6, "out6");
    glBindFragDataLocation(this->d_program, 7, "out7");

    // did we linkt successful?
    glGetProgramiv(this->d_program, GL_LINK_STATUS, &result);
    glGetProgramiv(this->d_program, GL_INFO_LOG_LENGTH, &infoLogLength);

    // got some errors?
    if (infoLogLength > 0)
    {
        std::vector<char> programErrorMessage(infoLogLength + 1);

        glGetProgramInfoLog(this->d_program, infoLogLength, NULL, &programErrorMessage[0]);

        Log::log("OpenGL reported the following errors:\n\t" + std::string(&programErrorMessage[0]));
    }

    // shaders are linked into the program, so we can delete them
    glDetachShader(this->d_program, vertexShaderId);
    glDetachShader(this->d_program, fragmentShaderId);

    glDeleteShader(vertexShaderId);
    glDeleteShader(fragmentShaderId);
}

Shader::~Shader()
{

}

void Shader::bind() const
{
    // use our shader
    glUseProgram(this->d_program);
}

void Shader::unbind() const
{
    // use default (= no) shader
    glUseProgram(0);
}

GLuint Shader::getAttribLocation(const std::string& attrib) const
{
    // return the location of "attrib" in the shader
    return glGetAttribLocation(this->d_program, attrib.c_str());
}

GLuint Shader::getUniformLocation(const std::string &attrib) const
{
    // return location of "attrib" in the shader
    return glGetUniformLocation(this->d_program, attrib.c_str());
}
