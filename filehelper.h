#ifndef FILEHELPER_H
#define FILEHELPER_H

#include <fstream>
#include <string>
#include <vector>

class FileHelper
{
public:

    // reads in a file's content line by line
    static std::vector<std::string> getFileContents(const std::string& path)
    {
        std::vector<std::string> content;
        std::string line;
        std::ifstream file;

        file.open(path);

        if (!file.is_open())
        {
            return content;
        }

        while (std::getline(file, line))
        {
            content.push_back(line);
        }

        file.close();

        return content;
    }

private:
    FileHelper();
    ~FileHelper();
};

#endif // FILEHELPER_H
