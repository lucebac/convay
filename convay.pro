TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    gui.cpp \
    sdl.cpp \
    log.cpp \
    game.cpp \
    cell.cpp \
    shader.cpp

include(deployment.pri)
qtcAddDeployment()

INCLUDEPATH += /home/lucebac/dev/cegui_v0-8/cegui/include /home/lucebac/build/cegui_v0-8/cegui/include

LIBS += -L/home/lucebac/build/cegui_v0-8/lib -lCEGUIBase-0 -lCEGUIOpenGLRenderer-0 -lCEGUISDL2ImageCodec -lGL -lSDL2 -lGLEW

HEADERS += \
    sdl_scancode_to_dinput_mappings.h \
    gui.h \
    sdl.h \
    log.h \
    game.h \
    globals.h \
    cell.h \
    shader.h \
    filehelper.h

QMAKE_CXXFLAGS += -std=c++11

debug {
    DEFINES += _DEBUG
}

