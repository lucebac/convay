#include "gui.h"

#include <CEGUI/CEGUI.h>
#include <CEGUI/RendererModules/OpenGL/GL3Renderer.h>

#include <SDL2/SDL_events.h>

#if !defined(CEGUI_USE_GLEW)
#include <GL/gl.h>
#endif

#include <iostream>
#include <map>
#include <random>
#include <sstream>
#include <string>

#include <time.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "globals.h"
#include "log.h"
#include "sdl.h"
#include "game.h"
#include "shader.h"
#include "filehelper.h"

using namespace CEGUI;

Gui::Gui()
{
    // initalize the PRNG with something different each time the program starts
    std::srand(::time(NULL));
}

Gui::~Gui()
{

}

void Gui::destroy()
{
    // shutdown cegui
    System::destroy();

    Log::log("[ OK ] destroy system");

    // if we have a shader (remaining), delete it
    if (this->d_shader)
    {
        delete this->d_shader;
        this->d_shader = nullptr;
    }

    // if there is a game remaining, delete it
    if (this->d_game)
    {
        delete this->d_game;
        this->d_game = nullptr;
    }

    // shutdown renderer
    OpenGL3Renderer::destroy(*this->d_renderer);
    this->d_renderer = nullptr;

    Log::log("[ OK ] destroy renderer");
}

void Gui::setupRenderer()
{
    // create renderer and enable extra states
    this->d_renderer = &OpenGL3Renderer::create(Sizef(
        static_cast<float>(this->d_screenWidth), static_cast<float>(this->d_screenHeight)
    ));

    // setup shader etc
    d_shader = new Shader();

    // get matrix location
    this->d_matrixLocation = this->d_shader->getUniformLocation("mvp");

    // create VAO
    glGenVertexArrays(1, &this->d_vao);

    // create VBO
    glGenBuffers(1, &this->d_vbo);
    glGenBuffers(1, &this->d_cbo);

    // calulate initial ModevViewPerspection matrix
    glm::mat4 projection = glm::perspective(glm::radians(45.0f), 1280.f / 720.f, 0.1f, 100.0f);
    glm::mat4 view = glm::lookAt(
                   glm::vec3(12.8,12.8,-1),
                   glm::vec3(12.8,12.8,0),
                   glm::vec3(0,1,0)
    );

    // setup default mvp
    this->d_mvp = projection * view;

    Log::log("[ OK ] created OpenGLRenderer");
}

void Gui::initCEGUI()
{
    // create CEGUI system object
    System::create(*this->d_renderer);

    Log::log("[ OK ] created System");

    // setup resource directories
    DefaultResourceProvider* rp = static_cast<DefaultResourceProvider*>(System::getSingleton().getResourceProvider());
    rp->setResourceGroupDirectory("schemes", "datafiles/schemes/");
    rp->setResourceGroupDirectory("imagesets", "datafiles/imagesets/");
    rp->setResourceGroupDirectory("fonts", "datafiles/fonts/");
    rp->setResourceGroupDirectory("layouts", "datafiles/layouts/");
    rp->setResourceGroupDirectory("looknfeels", "datafiles/looknfeel/");
    rp->setResourceGroupDirectory("lua_scripts", "datafiles/lua_scripts/");
    rp->setResourceGroupDirectory("schemas", "datafiles/xml_schemas/");

    Log::log("[ OK ] setup resource directories");

    // set default resource groups
    ImageManager::setImagesetDefaultResourceGroup("imagesets");
    Font::setDefaultResourceGroup("fonts");
    Scheme::setDefaultResourceGroup("schemes");
    WidgetLookManager::setDefaultResourceGroup("looknfeels");
    WindowManager::setDefaultResourceGroup("layouts");
    ScriptModule::setDefaultResourceGroup("lua_scripts");

    XMLParser* parser = System::getSingleton().getXMLParser();
    if (parser->isPropertyPresent("SchemaDefaultResourceGroup"))
        parser->setProperty("SchemaDefaultResourceGroup", "schemas");

    Log::log("[ OK ] setup default resource groups");

    // load TaharezLook scheme and DejaVuSans-10 font
    SchemeManager::getSingleton().createFromFile("AlfiskoSkin.scheme", "schemes");
    FontManager::getSingleton().createFromFile("DejaVuSans-10.font");

    Log::log("[ OK ] loaded font");

    // set default font and cursor image and tooltip type
    System::getSingleton().getDefaultGUIContext().setDefaultFont("DejaVuSans-10");
    System::getSingleton().getDefaultGUIContext().getMouseCursor().setDefaultImage("AlfiskoSkin/MouseArrow");

    Log::log("[ OK ] setup default mouse cursor and font");
}

void Gui::initWindows()
{
    // load layout(s)
    this->d_overlay = WindowManager::getSingleton().loadLayoutFromFile("convay_overlay.layout");
    System::getSingleton().getDefaultGUIContext().setRootWindow(this->d_overlay);

    Log::log("[ OK ] loaded window layout(s)");
}

void Gui::initCallbacks()
{
    // in general, callbacks get called if a certain event was emit by some window componen.
    // here, we just tell CEGUI which funtion to call for which event

    // handles a movement on the slider's bar
    Slider* tick_duration_slider = static_cast<Slider*>(this->d_overlay->getChild("SettingsWindow/TickDurationSlider"));
    tick_duration_slider->setCurrentValue(128.0);
    tick_duration_slider->subscribeEvent(Slider::EventValueChanged, Event::Subscriber(
        &Gui::durationSliderHandler, this));

    // "random seed button" down
    PushButton* random_seed_button = static_cast<PushButton*>(this->d_overlay->getChild("SettingsWindow/RandomSeedButton"));
    random_seed_button->subscribeEvent(PushButton::EventClicked, Event::Subscriber(
        &Gui::randomSeedHandler, this));

    // "start button" down
    PushButton* start_button = static_cast<PushButton*>(this->d_overlay->getChild("StartButton"));
    start_button->subscribeEvent(PushButton::EventClicked, Event::Subscriber(
        &Gui::startHandler, this));

    // "stop button" down
    PushButton* stop_button = static_cast<PushButton*>(this->d_overlay->getChild("StopButton"));
    stop_button->subscribeEvent(PushButton::EventClicked, Event::Subscriber(
        &Gui::stopHandler, this));

    // "pause button" down
    PushButton* pause_button = static_cast<PushButton*>(this->d_overlay->getChild("PauseButton"));
    pause_button->subscribeEvent(PushButton::EventClicked, Event::Subscriber(
        &Gui::pauseHandler, this));

    // "exit button" down
    PushButton* exit_button = static_cast<PushButton*>(this->d_overlay->getChild("ExitButton"));
    exit_button->subscribeEvent(PushButton::EventClicked, Event::Subscriber(
        &Gui::exitHandler, this));

    // "apply button" down
    PushButton* load_seed_button = static_cast<PushButton*>(this->d_overlay->getChild("SettingsWindow/ApplySeedButton"));
    load_seed_button->subscribeEvent(PushButton::EventClicked, Event::Subscriber(
        &Gui::loadSeedHandler, this));

    // "load map from file button" down
    PushButton* load_file_button = static_cast<PushButton*>(this->d_overlay->getChild("SettingsWindow/LoadFileButton"));
    load_file_button->subscribeEvent(PushButton::EventClicked, Event::Subscriber(
        &Gui::loadFileHandler, this));

    // global key listener for F1
    this->d_overlay->subscribeEvent(Window::EventKeyDown, Event::Subscriber(
        &Gui::helpKeyHandler, this));
}

void Gui::setupGame(Game* game)
{
    // save the game instance to somewhere we can access it
    this->d_game = game;
    // initialize a new game and tell it the board size (static for now, though)
    this->d_game->init(Game::StatusNew, this->d_size);

    // how many vertices do we have?
    this->d_vertexCount = this->d_game->getVertexCount();

    // bind vertices
    glBindVertexArray(this->d_vao);
    glBindBuffer(GL_ARRAY_BUFFER, this->d_vbo);
    glBufferData(GL_ARRAY_BUFFER, this->d_vertexCount * sizeof(float),
                 this->d_game->getVertexBufferRaw(), GL_STATIC_DRAW);
}

void Gui::tick(float time_since_last_frame)
{
    // save frame time
    this->d_deltaTime = time_since_last_frame;

    if (!this->d_game->isPaused()
        && this->d_game->isRunning())
    {
        // how long has it been since the last tick?
        this->d_timeSinceLastTick += time_since_last_frame;

        // if too long, we need to do some logic then
        if (this->d_timeSinceLastTick > this->d_tickDuration)
        {
            // how many ticks did we miss?
            int num_times = static_cast<int>(this->d_timeSinceLastTick / this->d_tickDuration);

            this->d_timeSinceLastTick -= (this->d_tickDuration * num_times);

            // do the logic
            DO_X_TIMES(num_times)
            {
                this->d_game->tick();
            }
        }
    }
}

void Gui::renderGame()
{
    // use our VAO
    glBindVertexArray(this->d_vao);

    // apply our shader
    this->d_shader->bind();

    // matrix changed? recalc and tell opengl then
    if (!this->d_matrixValid)
    {
        updateMVP();

        glUniformMatrix4fv(this->d_matrixLocation, 1, GL_FALSE, glm::value_ptr(this->d_mvp));
    }

    // vertices fist
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, this->d_vbo);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void *) 0);

    // then colors
    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, this->d_cbo);
    glBufferData(GL_ARRAY_BUFFER, this->d_game->getColorCount() * sizeof(float),
                 this->d_game->getColorBufferRaw(), GL_DYNAMIC_DRAW);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void *) 0);

    // draw the vertices
    glDrawArrays(GL_TRIANGLES, 0, this->d_vertexCount);

    // finished coloring
    glDisableVertexAttribArray(1);
    // no vertices left
    glDisableVertexAttribArray(0);

    // reset to default shader
    this->d_shader->unbind();
}

void Gui::handleInput()
{
    SDL_Event event;

    System& system = System::getSingleton();

    // query and process events
    while (SDL_PollEvent(&event))
    {
        switch (event.type)
        {
        case SDL_QUIT: // window "X" was clicked
            this->d_quit_requested = true;
            break;

        case SDL_MOUSEMOTION: // mouse was moved
            system.getDefaultGUIContext().injectMousePosition(static_cast<float>(event.motion.x),
                                                              static_cast<float>(event.motion.y));
            break;

        case SDL_MOUSEBUTTONDOWN: // some mouse button was pressed
            system.getDefaultGUIContext().injectMouseButtonDown(SDL::toCEGUIMouseButton(event.button.button));
            break;

        case SDL_MOUSEBUTTONUP: // some mouse button was released
            system.getDefaultGUIContext().injectMouseButtonUp(SDL::toCEGUIMouseButton(event.button.button));
            break;

        case SDL_MOUSEWHEEL: // someone turned the mouse wheel
            system.getDefaultGUIContext().injectMouseWheelChange(static_cast<float>(event.wheel.y));
            break;

        case SDL_KEYDOWN: // key down
        {
            if (event.key.keysym.sym == SDLK_q
                && (event.key.keysym.mod & KMOD_LCTRL
                    || event.key.keysym.mod & KMOD_RCTRL))
            {
                // user wants to quit
                this->d_quit_requested = true;
                break;
            }

            // internally mark key as pressed
            this->d_keyPressed[event.key.keysym.sym] = true;

            // tell CEGUI that some key was pressed down
            system.getDefaultGUIContext().injectKeyDown(SDL::toCEGUIKey(event.key.keysym.scancode));

            break;
        }

        case SDL_KEYUP:
            // internally mark this key as released
            this->d_keyPressed[event.key.keysym.sym] = false;

            // tell CEGUI that a key was released
            system.getDefaultGUIContext().injectKeyUp(SDL::toCEGUIKey(event.key.keysym.scancode));
            break;

        case SDL_TEXTINPUT: // some printable char's key was pressed (yes, that's a difference for SDL)
            SDL::injectUTF8Text(event.text.text);
            break;

        case SDL_WINDOWEVENT: // window was moved or resized or mouse left window
            if (event.window.event == SDL_WINDOWEVENT_RESIZED)
            {
                system.notifyDisplaySizeChanged(Sizef(static_cast<float>(event.window.data1),
                                                      static_cast<float>(event.window.data2)));

                this->d_screenWidth  = event.window.data1;
                this->d_screenHeight = event.window.data2;
            }
            else if (event.window.event == SDL_WINDOWEVENT_LEAVE)
            {
                system.getDefaultGUIContext().injectMouseLeaves();
            }
            break;

        default:
            break;

        }
    }

    // all events got processed, now actually handle the keypresses
    // it is important to do this apart from the above because if one
    // wanted scroll the board using key it would only scroll when the
    // key gets pressed or released - but not if the was held down
    if (this->d_keyPressed[SDLK_w])
    {
        this->scrollUp();
    }
    if (this->d_keyPressed[SDLK_a])
    {
        this->scrollRight();
    }
    if (this->d_keyPressed[SDLK_s])
    {
        this->scrollDown();
    }
    if (this->d_keyPressed[SDLK_d])
    {
        this->scrollLeft();
    }
    if (this->d_keyPressed[SDLK_e])
    {
        this->zoomIn();
    }
    if (this->d_keyPressed[SDLK_q])
    {
        this->zoomOut();
    }
}

bool Gui::durationSliderHandler(const EventArgs& e)
{
    // ignore "e"
    (void) e;

    // get the widgets we need to handle this event
    Slider* const slider = static_cast<Slider*>(this->d_overlay->getChild("SettingsWindow/TickDurationSlider"));
    Editbox* const box = static_cast<Editbox*>(this->d_overlay->getChild("SettingsWindow/TickDurationEdit"));

    // do some math to get rid of unnecessary precision
    this->d_tickDuration = (((int)(slider->getCurrentValue() * 100)) / 100.f);
    if (this->d_tickDuration < 0.05f)
    {
        // more than 20 ticks per second is evil! go and test it
        this->d_tickDuration = 0.05f;
        slider->setCurrentValue(0.05f);
    }
    // show the result in the window
    box->setText((std::to_string(this->d_tickDuration).substr(0, 4) + "s").c_str());

    return true;
}

bool Gui::startHandler(const EventArgs &e)
{
    (void) e;

    // update the button's "enabled" status
    this->d_overlay->getChild("StartButton")->setEnabled(false);
    this->d_overlay->getChild("StopButton")->setEnabled(true);
    this->d_overlay->getChild("PauseButton")->setEnabled(true);

    // prevent changes on the settings while game is not stopped
    this->d_overlay->getChild("SettingsWindow/RandomSeedButton")->disable();
    this->d_overlay->getChild("SettingsWindow/ApplySeedButton")->disable();
    this->d_overlay->getChild("SettingsWindow/LoadFileButton")->disable();

    // go for it!
    this->d_game->start();

    return true;
}

bool Gui::stopHandler(const EventArgs &e)
{
    (void) e;

    // update button status
    this->d_overlay->getChild("StartButton")->setEnabled(true);
    this->d_overlay->getChild("StopButton")->setEnabled(false);
    this->d_overlay->getChild("PauseButton")->setEnabled(false);

    // stop the game
    this->d_game->stop();

    // unlock the settings window
    this->d_overlay->getChild("SettingsWindow/RandomSeedButton")->enable();
    this->d_overlay->getChild("SettingsWindow/ApplySeedButton")->enable();
    this->d_overlay->getChild("SettingsWindow/LoadFileButton")->enable();

    return true;
}

bool Gui::pauseHandler(const EventArgs &e)
{
    (void) e;

    // update button status
    this->d_overlay->getChild("StartButton")->setEnabled(true);
    this->d_overlay->getChild("StopButton")->setEnabled(true);
    this->d_overlay->getChild("PauseButton")->setEnabled(false);

    // pause game
    this->d_game->pause();

    // lock settings widgets
    this->d_overlay->getChild("SettingsWindow/RandomSeedButton")->disable();
    this->d_overlay->getChild("SettingsWindow/ApplySeedButton")->disable();
    this->d_overlay->getChild("SettingsWindow/LoadFileButton")->disable();

    return true;
}

bool Gui::exitHandler(const EventArgs &e)
{
    (void) e;

    // user wants to quit
    this->d_quit_requested = true;

    return true;
}

bool Gui::randomSeedHandler(const EventArgs &e)
{
    (void) e;

    // get the widget
    Editbox* edit = static_cast<Editbox*>(this->d_overlay->getChild("SettingsWindow/SeedEdit"));
    edit->setText(std::to_string(std::rand()));

    // update the game's internal state
    return loadSeedHandler(e);
}

bool Gui::loadFileHandler(const EventArgs &e)
{
    (void) e;

    std::array<Cell::Status, 128 * 128> data;
    int idx = 0;

    // get the map's contents
    std::vector<std::string> content = FileHelper::getFileContents("datafiles/playground.map");

    // since we draw from bottom to top, we need to reverse the lines in the vector
    std::reverse(content.begin(), content.end());

    // parse the map line by line
    for (auto& line : content)
    {
        // ... and char by char
        for (auto& chr : line)
        {
            if (chr == '0') // '0' -> cell dead
            {
                data[idx++] = Cell::StatusDead;
            }
            else if (chr == '1') // '1' -> cell alive
            {
                data[idx++] = Cell::StatusAlive;
            }
            else
            {
                // not allowed
                Log::log(std::string("[FAIL] malformed map file: invalid char \"") + chr + "\"");
                return true;
            }
        }
    }

    // did we read exact 128 by 128 cells?
    if (idx != 128 * 128)
    {
        Log::log("[FAIL] malformed map file: not enough bytes");
        return true;
    }

    // update the game's internal state
    this->d_game->loadStates(data);

    return true;
}

bool Gui::loadSeedHandler(const EventArgs &e)
{
    (void) e;

    // get the seed
    std::string strnum(static_cast<Editbox*>(this->d_overlay->getChild("SettingsWindow/SeedEdit"))->getText().c_str());

    if (!strnum.empty())
    {
        // make int from string
        int seed = std::stoi(strnum);

        // tell the new seed and re-init the game
        this->d_game->setSeed(seed);
        this->d_game->init(Game::StatusRandom);
    }

    return true;
}

bool Gui::helpKeyHandler(const EventArgs &e)
{
    const KeyEventArgs &args = static_cast<const KeyEventArgs&>(e);

    // if F1 was pressed then toggle the help window
    if (args.scancode == CEGUI::Key::F1)
    {
        FrameWindow* const window = static_cast<FrameWindow*>(
                    this->d_overlay->getChild("HelpWindow"));

        window->setRolledup(!window->isRolledup());
    }

    return true;
}

void Gui::scrollLeft()
{
    this->d_position -= this->d_right * this->d_scrollSpeed * this->d_deltaTime;
    this->d_matrixValid = false;
}

void Gui::scrollRight()
{
    this->d_position += this->d_right * this->d_scrollSpeed * this->d_deltaTime;
    this->d_matrixValid = false;
}

void Gui::scrollUp()
{
    this->d_position += this->d_up * this->d_scrollSpeed * this->d_deltaTime;
    this->d_matrixValid = false;
}

void Gui::scrollDown()
{
    this->d_position -= this->d_up * this->d_scrollSpeed * this->d_deltaTime;
    this->d_matrixValid = false;
}

void Gui::zoomIn()
{
    updateZoomFactor(+1.f);
}

void Gui::zoomOut()
{
    updateZoomFactor(-1.f);
}

void Gui::updateZoomFactor(float zoomFactor)
{
    if (zoomFactor < 0)
    {
        // zoom out
        this->d_position -= d_front * 0.25f;
    }
    else
    {
        // zoom in
        this->d_position += d_front * 0.25f;
    }

    this->d_matrixValid = false;
}

void Gui::updateMVP()
{
    // calculate our viewport
    glm::mat4 projection = glm::perspective(
        45.f,
        static_cast<float>(this->d_screenWidth) / this->d_screenHeight,
        0.1f,
        100.f);

    // what at we looking at? in our case we just stare at the board
    glm::mat4 view = glm::lookAt(
        this->d_position,
        this->d_position - glm::vec3(0, 0, 1),
        glm::vec3(0, 1, 0)
    );

    // calculate mvp
    this->d_mvp = projection * view;
    // matrix is valid now
    this->d_matrixValid = true;
}
