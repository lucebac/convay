#ifndef LOG_H
#define LOG_H

#ifndef _DEBUG
#  include <iostream>
#else
#  include <fstream>
#endif
#include <string>

class Log
{
public:
    // normal log message
    static void log(const std::string& str);
    static void log(const char* str);

    // error message
    static void err(const std::string& str);
    static void err(const char* str);

private:
    Log();
};

#endif // LOG_H
