#ifndef CELL
#define CELL

class Cell
{
public:
    typedef enum
    {
        StatusDead = 0,
        StatusAlive = 1
    } Status;

    Cell();
    Cell(int xPos, int yPos, int boardWidth, Status status);

private:
    int d_xPos        = -1;
    int d_yPos        = -1;
    int d_boardWidth  = 128;
    Status d_status   = StatusDead;


public: // getters and setters
    void setStatus(Status status)
    { this->d_status = status; }

    Status getStatus() const
    { return this->d_status; }

    bool isAlive() const
    { return this->d_status == StatusAlive; }

    int getXPos() const
    { return this->d_xPos; }

    void setXPos(int xPos)
    { this->d_xPos = xPos; }

    int getYPos() const
    { return this->d_yPos; }

    void setYPos(int yPos)
    { this->d_yPos = yPos; }

    int getIndex() const
    { return this->d_yPos * this->d_boardWidth + this->d_xPos; }

    // index of {top,bottom,left,right} cell
    int getTopIndex() const;
    int getBottomIndex() const;
    int getLeftIndex() const;
    int getRightIndex() const;
};

#endif // CELL

