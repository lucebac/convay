#include "log.h"

#include <iostream>

Log::Log()
{

}

void Log::log(const std::string& str)
{
    // print message to stdout
    std::cout << str << std::endl;
}

void Log::log(const char* str)
{
    // wrapper function
    log(std::string(str));
}

void Log::err(const std::string& str)
{
    // print message to stderr
    std::cerr << str << std::endl;
}

void Log::err(const char* str)
{
    // wrapper function
    log(std::string(str));
}
