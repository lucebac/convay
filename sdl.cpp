#include "sdl.h"

#include <SDL2/SDL.h>

#include <CEGUI/Config.h>
#if !defined(CEGUI_USE_GLEW)
#include <GL/gl.h>
#endif

#include "log.h"

SDL_Window* SDL::d_window = NULL;
SDL_GLContext SDL::d_context;

SDL::SDL()
{

}

void SDL::setup()
{
    // init everything from SDL
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
    {
        Log::log("[FAIL] initialize SDL");
        Log::err("SDL could not be initialized!");
        Log::err(std::string("Error message: ") + SDL_GetError());

        exit(1);
    }

    Log::log("[ OK ] initialize SDL");

    // create opengl window
    d_window = SDL_CreateWindow("Convay's Game Of Life", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                1280, 720, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
    // no window was created
    if (!d_window)
    {
        Log::log("[FAIL] create SDL window");
        Log::err(std::string("Could not create SDL window: ") + SDL_GetError());

        SDL_Quit();
        exit(1);
    }

    Log::log("[ OK ] create SDL window");

    // disable native mouse cursor
    SDL_ShowCursor(0);

    // setup opengl rendering context
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

    // tell opengl we want to use it to render
    d_context = SDL_GL_CreateContext(d_window);

    Log::log("[ OK ] create GL context");
}

void SDL::quit()
{
    // shutdown opengl
    SDL_GL_DeleteContext(d_context);
    // close the window
    SDL_DestroyWindow(d_window);
    // shutdown SDL
    SDL_Quit();

    Log::log("[ OK ] shutdown SDL2");
}
