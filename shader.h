#ifndef SHADER_H
#define SHADER_H

#include <GL/glew.h>

#include <string>

class Shader
{
public:
    // ctor - compile shader and link shader program
    Shader();
    // dtor - unlink shader from opengl
    ~Shader();

    // set our shader as active
    void bind() const;
    // release our shader
    void unbind() const;

    // get location of "attrib" in the program
    GLuint getAttribLocation(const std::string& attrib) const;
    GLuint getUniformLocation(const std::string& attrib) const;

private:
    GLuint d_program = 0; // opengl internal program id
};

#endif // SHADER_H
