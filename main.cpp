/**************************************************************************
* The following libs (and corresponding headers) are needed to compile and to link:
* CEGUIBase
* CEGUIOpenGLRenderer
* CEGUICoreWindowRendererSet
* default CEGUI xml parser (and dependencies)
* SDL2main (windows only)
* SDL2
* OpengGL
* glm headers (as part of CEGUIBase)
***************************************************************************/

#include <iostream>

#include <CEGUI/CEGUI.h>
#include <CEGUI/RendererModules/OpenGL/GLRenderer.h>

#include <SDL2/SDL.h>

#if !defined(CEGUI_USE_GLEW)
#include <GL/gl.h>
#endif

#include "globals.h"
#include "gui.h"
#include "sdl.h"
#include "log.h"
#include "game.h"

int main(int argc, char* argv[])
{
    (void) argc;
    (void) argv;

    using namespace CEGUI;

    // setup GL and key/mouse input
    SDL::setup();

    // setup gui
    Gui* gui = new Gui();
    gui->setupRenderer();
    gui->initCEGUI();
    gui->initWindows();
    gui->initCallbacks();
    gui->setupGame(new Game());

    // notify system of the window size
    System::getSingleton().notifyDisplaySizeChanged(Sizef(1280.f, 720.f));

    // set gl clear color
    glClearColor(0.1f, 0.5f, 0.2f, 1.f);

    float time = SDL_GetTicks() / 1000.f;

    // repeat until a quit is requested
    while (!gui->quitRequested() && !SDL_QuitRequested())
    {

        // inject time pulses
        const float newtime = SDL_GetTicks() / 1000.f;
        const float time_elapsed = newtime - time;
        System::getSingleton().injectTimePulse(time_elapsed);
        System::getSingleton().getDefaultGUIContext().injectTimePulse(time_elapsed);
        time = newtime;

        // send elapsed time to the game and render the state
        gui->tick(time_elapsed);

        // poll all events and handle them
        gui->handleInput();

        // clear screen
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // render our game
        gui->renderGame();

        // render gui
        gui->getRenderer()->beginRendering();
        System::getSingleton().renderAllGUIContexts();
        gui->getRenderer()->endRendering();

        // swap buffers
        SDL::swapWindows();

        // any opengl errors?
        handleGlErrors(log);
    }

    // destroy system and renderer
    gui->destroy();

    // shutdown opengl
    SDL::quit();

    return 0;
}

// for reasons, SDL emits TEXTINPUT event when a (printable) char key was pressed
// luckily, Daniel Gibson contributed a conversion function to CEGUI that passes the correct values
void SDL::injectUTF8Text(const char* utf8str)
{
    static SDL_iconv_t cd = SDL_iconv_t(-1);

    if (cd == SDL_iconv_t(-1))
    {
        // note: just "UTF-32" doesn't work as toFormat, because then you get BOMs, which we don't want.
        const char* toFormat = "UTF-32LE"; // TODO: what does CEGUI expect on big endian machines?
        cd = SDL_iconv_open(toFormat, "UTF-8");
        if (cd == SDL_iconv_t(-1))
        {
            Log::err("Couldn't initialize SDL_iconv for UTF-8 to UTF-32!");
            return;
        }
    }

    // utf8str has at most SDL_TEXTINPUTEVENT_TEXT_SIZE (32) chars,
    // so we won't have have more utf32 chars than that
    Uint32 utf32buf[SDL_TEXTINPUTEVENT_TEXT_SIZE] = {0};

    // we'll convert utf8str to a utf32 string, saved in utf32buf.
    // the utf32 chars will be injected into cegui

    size_t len = strlen(utf8str);

    size_t inbytesleft = len;
    size_t outbytesleft = 4 * SDL_TEXTINPUTEVENT_TEXT_SIZE; // *4 because utf-32 needs 4x as much space as utf-8
    char* outbuf = (char*)utf32buf;
    size_t n = SDL_iconv(cd, &utf8str, &inbytesleft, &outbuf, &outbytesleft);

    if (n == size_t(-1)) // some error occured during iconv
    {
        Log::err(std::string("Converting UTF-8 string ") + utf8str + " from SDL_TEXTINPUT to UTF-32 failed!");
    }

    for (int i = 0; i < SDL_TEXTINPUTEVENT_TEXT_SIZE; ++i)
    {
        if (utf32buf[i] == 0)
            break; // end of string

        CEGUI::System::getSingleton().getDefaultGUIContext().injectChar(utf32buf[i]);
    }

    // reset cd so it can be used again
    SDL_iconv(cd, NULL, &inbytesleft, NULL, &outbytesleft);
}
