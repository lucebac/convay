#include "game.h"

#include <CEGUI/Config.h>
#if !defined(CEGUI_USE_GLEW)
#include <GL/gl.h>
#endif

#include <array>
#include <vector>
#include <algorithm>

#include "globals.h"
#include "gui.h"
#include "cell.h"


Game::Game()
{

}

Game::~Game()
{

}

void Game::init(Status status, int boardWidth)
{
    // update board only when a really new game made
    if (status == StatusNew)
    {
        this->d_boardWidth = boardWidth;
    }

    std::vector<float> vertexBuffer(128 * 128 * 6 * 3);
    std::vector<float> vertexColors(128 * 128 * 6 * 3);
    std::vector<Cell>  cells(128 * 128);

    if (status == StatusRandom)
    {
        // initialize PRNG with given seed
        std::srand(this->d_seed);
    }

    // loop through all cells
    for (int i = 0; i < 128 * 128; ++i)
    {
        const int offset = i * 18;

        // luckily, board width is a multiple of 2 so we can use bit operations instead of modulo and integer division
        // to speed up calculation. should not have a huge impact in general, though, since we do initialization only once.
        int x = i & 0x7f;
        int y = (i >> 7) & 0x7f;

        // do we have a living or a dead cell?
        Cell::Status status = (std::rand() & 1) ? Cell::StatusAlive : Cell::StatusDead;
        float color = static_cast<float>(status == Cell::StatusAlive);

        // create our cell
        Cell c(x, y, 128, status);
        cells[i] = c;

        // vertices

        // bottom left
        vertexBuffer[offset +  0] = x * 0.1f;
        vertexBuffer[offset +  1] = y * 0.1f;
        vertexBuffer[offset +  2] = 0.f;
        // bottom right
        vertexBuffer[offset +  3] = (x + 1) * 0.1f;
        vertexBuffer[offset +  4] = y * 0.1f;
        vertexBuffer[offset +  5] = 0.f;
        // top left
        vertexBuffer[offset +  6] = x * 0.1f;
        vertexBuffer[offset +  7] = (y + 1) * 0.1f;
        vertexBuffer[offset +  8] = 0.f;
        // bottom right
        vertexBuffer[offset +  9] = (x + 1) * 0.1f;
        vertexBuffer[offset + 10] = y * 0.1f;
        vertexBuffer[offset + 11] = 0.f;
        // top right
        vertexBuffer[offset + 12] = (x + 1) * 0.1f;
        vertexBuffer[offset + 13] = (y + 1) * 0.1f;
        vertexBuffer[offset + 14] = 0.f;
        // top left
        vertexBuffer[offset + 15] = x * 0.1f;
        vertexBuffer[offset + 16] = (y + 1) * 0.1f;
        vertexBuffer[offset + 17] = 0.f;

        // colors

        // bottom left
        vertexColors[offset +  0] = vertexColors[offset +  1] = vertexColors[offset +  2] = color;
        // bottom right
        vertexColors[offset +  3] = vertexColors[offset +  4] = vertexColors[offset +  5] = color;
        // top left
        vertexColors[offset +  6] = vertexColors[offset +  7] = vertexColors[offset +  8] = color;
        // bottom right
        vertexColors[offset +  9] = vertexColors[offset + 10] = vertexColors[offset + 11] = color;
        // top right
        vertexColors[offset + 12] = vertexColors[offset + 13] = vertexColors[offset + 14] = color;
        // top left
        vertexColors[offset + 15] = vertexColors[offset + 16] = vertexColors[offset + 17] = color;
    }

    // save all the vertices, colors, and cells
    this->d_vertexBuffer = std::move(vertexBuffer);
    this->d_vertexColors = std::move(vertexColors);
    this->d_cells        = std::move(cells);
}

void Game::loadStates(const std::array<Cell::Status, 128 * 128>& status)
{
    auto cell_begin = this->d_cells.begin();
    auto cell_end   = this->d_cells.end();

    auto status_begin = status.cbegin();
    auto status_end   = status.cend();

    // loop through all cells and set the status to the one that was supplied for each cell
    for (; cell_begin != cell_end && status_begin != status_end; ++cell_begin, ++status_begin)
    {
        (*cell_begin).setStatus(*status_begin);
    }

    // we need to recalculate the colors now
    updateColors();
}

void Game::tick()
{
    std::vector<Cell>  new_cells(128 * 128);
    std::vector<float> new_colors(128 * 128 * 6 * 3);

    auto idx   = this->d_cells.cbegin();
    auto end   = this->d_cells.cend();
    int i      = 0;
    int cell_i = 0;

    // loop through all cells
    for (; idx != end; ++idx, i += 18, ++cell_i)
    {
        Cell cell = *idx;

        Cell::Status status = cell.getStatus();

        float color = static_cast<float>(status == Cell::StatusAlive);

        int neighbours_alive = 0;

        // count neighbours
        for (Cell neighbour : getAllCellNeighbours(cell))
        {
            neighbours_alive += static_cast<int>(neighbour.getStatus());
        }

        // apply rules
        if (neighbours_alive == 3 && cell.getStatus() == Cell::StatusDead)
        {
            cell.setStatus(Cell::StatusAlive);
            color = 1.f;
        }
        else if (neighbours_alive < 2 || neighbours_alive > 3)
        {
            cell.setStatus(Cell::StatusDead);
            color = 0.f;
        }

        // colors

        // bottom left
        new_colors[i +  0] = new_colors[i +  1] = new_colors[i +  2] = color;
        // bottom right
        new_colors[i +  3] = new_colors[i +  4] = new_colors[i +  5] = color;
        // top left
        new_colors[i +  6] = new_colors[i +  7] = new_colors[i +  8] = color;
        // bottom right
        new_colors[i +  9] = new_colors[i + 10] = new_colors[i + 11] = color;
        // top right
        new_colors[i + 12] = new_colors[i + 13] = new_colors[i + 14] = color;
        // top left
        new_colors[i + 15] = new_colors[i + 16] = new_colors[i + 17] = color;

        new_cells[cell_i] = cell;
    }

    // save the new colors and cells
    this->d_vertexColors = std::move(new_colors);
    this->d_cells        = std::move(new_cells);
}

void Game::start()
{
    if (!this->d_running)
    {
        // only backup cells of we have a fresh game
        this->d_cellsBackup = this->d_cells;
    }

    // play the game
    this->d_paused = false;
    this->d_running = true;
}

void Game::stop()
{
    // stop the game
    this->d_running = false;
    this->d_paused = false;

    // restore backup
    reset();
}

void Game::pause()
{
    this->d_paused = true;
}

std::vector<Cell> Game::getAllCellNeighbours(const Cell& base) const
{
    std::vector<Cell> cells;

    Cell top = this->d_cells.at(base.getTopIndex());
    Cell bottom = this->d_cells.at(base.getBottomIndex());

    cells.push_back(this->d_cells.at(top.getLeftIndex()));      // top left cell
    cells.push_back(top);                                       // top middle cell
    cells.push_back(this->d_cells.at(top.getRightIndex()));     // top right cell

    cells.push_back(this->d_cells.at(base.getLeftIndex()));     // left cell
    cells.push_back(this->d_cells.at(base.getRightIndex()));    // right cell

    cells.push_back(this->d_cells.at(bottom.getLeftIndex()));   // top left cell
    cells.push_back(bottom);                                    // top middle cell
    cells.push_back(this->d_cells.at(bottom.getRightIndex()));  // top right cell

    return cells;
}

void Game::updateColors()
{
    std::vector<float> new_colors(128 * 128 * 6 * 3);

    auto cell  = this->d_cells.cbegin();
    auto end   = this->d_cells.cend();
    int i      = 0;

    // loop through all cells and set the color according to the status
    for (; cell != end; ++cell, i += 18)
    {
        float color = static_cast<float>((*cell).getStatus() == Cell::StatusAlive);

        // bottom left
        new_colors[i +  0] = new_colors[i +  1] = new_colors[i +  2] = color;
        // bottom right
        new_colors[i +  3] = new_colors[i +  4] = new_colors[i +  5] = color;
        // top left
        new_colors[i +  6] = new_colors[i +  7] = new_colors[i +  8] = color;
        // bottom right
        new_colors[i +  9] = new_colors[i + 10] = new_colors[i + 11] = color;
        // top right
        new_colors[i + 12] = new_colors[i + 13] = new_colors[i + 14] = color;
        // top left
        new_colors[i + 15] = new_colors[i + 16] = new_colors[i + 17] = color;
    }

    // save the new colors
    this->d_vertexColors = std::move(new_colors);
}

void Game::reset()
{
    // restore backup
    this->d_cells = this->d_cellsBackup;

    // recalculate colors
    updateColors();
}
