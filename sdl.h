#ifndef SDL_H
#define SDL_H

#include <CEGUI/InputEvent.h>

#include <SDL2/SDL.h>

#include <CEGUI/Config.h>
#if !defined(CEGUI_USE_GLEW)
#include <GL/gl.h>
#endif

#include "sdl_scancode_to_dinput_mappings.h"

class SDL
{
private:
    static SDL_Window*   d_window;  // window handle
    static SDL_GLContext d_context; // glcontext handle

public:
    // setup SDL and create opengl context
    static void setup();
    // shutdown SDL and release opengl context
    static void quit();

    // SDL has a new TEXTINPUT event that enforces to handle
    // keypresses on printable chars a little bit different
    static void injectUTF8Text(const char* utf8str);

    // swap double buffers and bring the new one to the front
    static void swapWindows()
    { SDL_GL_SwapWindow(d_window); }

    // converter function
    static CEGUI::Key::Scan toCEGUIKey(SDL_Scancode key)
    { return static_cast<CEGUI::Key::Scan>(scanCodeToKeyNum[static_cast<int>(key)]); }

    // convert SDL mouse button to CEGUI mouse button
    static CEGUI::MouseButton toCEGUIMouseButton(const Uint8& button)
    {
        using namespace CEGUI;

        switch (button)
        {
        case SDL_BUTTON_LEFT:
            return LeftButton;

        case SDL_BUTTON_MIDDLE:
            return MiddleButton;

        case SDL_BUTTON_RIGHT:
            return RightButton;

        case SDL_BUTTON_X1:
            return X1Button;

        case SDL_BUTTON_X2:
            return X2Button;

        default:
            return NoButton;
        }
    }

private:
    SDL();
};

#endif // SDL_H
