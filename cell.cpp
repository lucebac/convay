#include "cell.h"

Cell::Cell()
{

}

Cell::Cell(int xPos, int yPos, int boardWidth, Status status):
    d_xPos(xPos)
  , d_yPos(yPos)
  , d_boardWidth(boardWidth)
  , d_status(status)
{

}

int Cell::getTopIndex() const
{
    int pos = (this->d_yPos + 1) * this->d_boardWidth;

    if (pos >= this->d_boardWidth * this->d_boardWidth)
    {
        pos -= this->d_boardWidth * this->d_boardWidth;
    }

    pos += this->d_xPos;

    return pos;
}

int Cell::getBottomIndex() const
{
    int pos = (this->d_yPos - 1) * this->d_boardWidth;

    if (pos < 0)
    {
        pos += this->d_boardWidth * this->d_boardWidth;
    }

    pos += this->d_xPos;

    return pos;
}

int Cell::getLeftIndex() const
{
    int pos = this->d_xPos - 1;

    if (pos < 0)
    {
        pos += this->d_boardWidth;
    }

    pos += this->d_yPos * this->d_boardWidth;

    return pos;
}

int Cell::getRightIndex() const
{
    int pos = this->d_xPos + 1;

    if (pos >= this->d_boardWidth)
    {
        pos -= this->d_boardWidth;
    }

    pos += this->d_yPos * this->d_boardWidth;

    return pos;
}
