#ifndef GLOBALS
#define GLOBALS

#include "log.h"

// print out opengl errors if there were some
#ifdef _DEBUG
#  define handleGlErrors(X) \
    while (GLenum err = glGetError())                    \
    {                                                    \
        Log::X("GLError (" + std::string(__FILE__) + ":" \
                   + std::to_string(__LINE__) + "): "    \
                   + std::to_string(err));               \
    }
#else
#  define handleGlErrors(X) ((void) Log::X)
#endif

// repeat x times
#define DO_X_TIMES(X) while (X-- > 0)

#endif // GLOBALS

