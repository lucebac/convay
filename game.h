#ifndef GAME_H
#define GAME_H

#include <array>
#include <vector>

#include "cell.h"

class Game
{
public:

    enum Status
    {
        StatusFile = 0,
        StatusRandom,
        StatusNew
    };

    // ctor
    Game();
    // dtor
    ~Game();

    // initalize vertices and colors
    void init(Status status, int boardWidth = -1);
    // set states of cells to those provided
    void loadStates(const std::array<Cell::Status, 128 * 128>& status);

    // apply rules to all cells
    void tick();

    // start/stop(reset)/pause game
    void start();
    void stop();
    void pause();

private:
    int   d_boardWidth   = 128; // height/width of board
    int   d_seed         = 0;   // initial seed for PRNG

    bool d_running = false;     // not running in the beginning
    bool d_paused  = false;     // not paused

    std::vector<Cell>  d_cells;        // list of all cells on the board (coordinates + status)
    std::vector<Cell>  d_cellsBackup;  // backup of cells to be able to reset the game
    std::vector<float> d_vertexBuffer; // 128 * 128 squares made of 2 traingles (and thus, 6 vertices made of 3 floats each)
    std::vector<float> d_vertexColors; // same goes for colors

    // returns a list of all neighbouring cells
    std::vector<Cell> getAllCellNeighbours(const Cell& base) const;
    // resets game to the state before the user clicked "play"
    void reset();
    // calculate colors based on all cells' status
    void updateColors();

public: // getters and setters
    const float* getVertexBufferRaw() const
    { return &this->d_vertexBuffer[0]; }

    const float* getColorBufferRaw() const
    { return &this->d_vertexColors[0]; }

    int getVertexCount() const
    { return this->d_vertexBuffer.size(); }

    int getColorCount() const
    { return this->d_vertexColors.size(); }

    bool isRunning() const
    { return this->d_running; }

    bool isPaused() const
    { return this->d_paused; }

    void setSeed(int seed)
    { this->d_seed = seed; }

};

#endif // GAME_H
