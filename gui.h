#ifndef GUI_H
#define GUI_H

#include <CEGUI/RendererModules/OpenGL/GL3Renderer.h>
#include <CEGUI/RendererModules/OpenGL/Shader.h>

#include <SDL2/SDL.h>

#include <glm/glm.hpp>

#include <map>

#include "game.h"
#include "shader.h"

class Gui
{
public:
    // ctor
    Gui();
    // dtor
    ~Gui();

    // clear everything up
    void destroy();

    // bring up cegui
    void initCEGUI();
    // create renderer and setup the OpenGL environment to fit our needs
    void setupRenderer();
    // load and initialize the gui
    void initWindows();
    // button callbacks
    void initCallbacks();

    // set game instance and setup opengl buffers
    void setupGame(Game* game);

    // pulls all events from SDL and handles them properly if we need to
    void handleInput();

    // mutate all cells
    void tick(float time_since_last_frame);
    // hand over the new vertex colors to opengl and render all our internal stuff
    void renderGame();

    // user clicked quit or hit ctrl+q
    bool quitRequested() const
    { return this->d_quit_requested; }

    // return board width/height
    int getSize() const
    { return this->d_size; }

    // button handler
    bool startHandler(const CEGUI::EventArgs& e);
    bool stopHandler(const CEGUI::EventArgs& e);
    bool pauseHandler(const CEGUI::EventArgs& e);
    bool exitHandler(const CEGUI::EventArgs& e);
    bool randomSeedHandler(const CEGUI::EventArgs& e);
    bool loadFileHandler(const CEGUI::EventArgs& e);
    bool loadSeedHandler(const CEGUI::EventArgs& e);

    // slider handler
    bool durationSliderHandler(const CEGUI::EventArgs& e);

    // keydown handler
    bool helpKeyHandler(const CEGUI::EventArgs& e);

    // return our renderer object
    CEGUI::OpenGL3Renderer* getRenderer() const
    { return this->d_renderer; }

    // scroll and zoom handlers
    // should be telling names
    void scrollLeft();
    void scrollRight();
    void scrollUp();
    void scrollDown();
    void zoomIn();
    void zoomOut();

    // actually updates the zoom factor - didn't guess that, huh?
    void updateZoomFactor(float zoomFactor);

    // updates the model view matrix e.g. when the player moved the camera
    void updateMVP();

private:
    Game*                   d_game       = nullptr; // no game
    CEGUI::Window*          d_overlay    = nullptr; // no windows
    CEGUI::OpenGL3Renderer* d_renderer   = nullptr; // no renderer
    Shader*                 d_shader     = nullptr; // no initial shader

    bool d_quit_requested               = false; // user clicked quit button or hit ctrl+q

    int   d_screenWidth       = 1280;  // actual window width, needed for viewport calculation etc
    int   d_screenHeight      = 720;   // actual window height, needed for viewport calculation etc
    int   d_size              = 128;   // board width and height
    float d_tickDuration      = 0.5;   // how long does a game tick take (in seconds)?
    float d_timeSinceLastTick = 0.f;   // time since last game tick (= cell mutation)
    float d_deltaTime         = 0.f;   // time since last frame
    float d_scrollSpeed       = 15.f;  // move at speed of 15 units per second

    GLuint    d_matrixLocation = 0; // we don't know the location of the mvp in the shader yet
    GLuint    d_vao            = 0; // VAO gets created later
    GLuint    d_vbo            = 0; // same goes for VBO
    GLuint    d_cbo            = 0; // and the color buffer
    GLsizei   d_vertexCount    = 0; // the number of vertices we created
    GLsizei   d_colorCount     = 0; // the number of colors we have in out color buffer

    glm::mat4       d_mvp           = glm::mat4(1.f);             // inital mvp is identity matrix
    glm::vec3       d_position      = glm::vec3(2.9, 6.8, 14.25); // position the board (acutally the camera, but doesn't matter here) at a good position
    const glm::vec3 d_right         = glm::vec3(1, 0, 0);         // define "right" direction  =|
    const glm::vec3 d_up            = glm::vec3(0, 1, 0);         // define "up" direction      |--> these would have changed if we allowed camera yaw etc
    const glm::vec3 d_front         = glm::vec3(0, 0, -1);        // define "front" direction  =|
    bool            d_matrixValid   = false;

    std::map<SDL_Keycode, bool> d_keyPressed; // store pressed and released keys
};

#endif // GUI_H
